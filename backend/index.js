const mysql = require('mysql');

const bcrypt = require('bcrypt');
const SALT_ROUNDS = 10

const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
dotenv.config()
const TOKEN_SECRET = process.env.TOKEN_SECRET

const connection = mysql.createConnection({
    host : 'localhost',
    user : 'hotel_admin',
    password : 'hotel_admin',
    database : 'HotelBookingSystem'
});

connection.connect();

const express = require('express')
const app = express()
const port = 5000

/* Middleware for Authenticating User Token */
function authenticateToken(req, res, next) {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if(token == null) return res.sendStatus(401)
    jwt.verify(token, TOKEN_SECRET, (err, user) => {
        if(err) { return res.sendStatus(403) }
        else {
            req.user = user
            next()
        }
    })
}

/* Register For A New Booker */
app.post("/register_booker", (req,res) => {
    let booker_name = req.query.booker_name
    let booker_surname = req.query.booker_surname
    let booker_username = req.query.booker_username
    let booker_password = req.query.booker_password

    bcrypt.hash(booker_password, SALT_ROUNDS, (err,hash) => {
        let query = `INSERT INTO Booker
                     (BookerName, BookerSurname, Username, Password)
                     VALUES ('${booker_name}','${booker_surname}',
                             '${booker_username}','${hash}')`
        //console.log(query)

        connection.query(query, (err,rows) => {
            if(err) {
                res.json({
                            "status" : "400",
                            "message" : "Error Cannot Register"
                })
            } else {
                res.json({
                            "status" : "200",
                            "message" : "Register Success"
                })
            }
        });
    })
})

/* Login */
app.post("/login", (req,res) => {
    let username = req.query.username
    let password = req.query.password

    let query = `SELECT * FROM Booker WHERE Username='${username}'`

    connection.query(query, (err,rows) => {
        if(err) {
            res.json({
                        "status" : "400",
                        "message" : "Error Querying From Database"
            })
        } else {
            let db_password = rows[0].Password
            bcrypt.compare(password, db_password, (err,result) => {
                if(result) {
                    let payload = {
                        "username" : rows[0].Username,
                        "user_id" : rows[0].BookerID
                    }
                    let token = jwt.sign(payload, TOKEN_SECRET, { expiresIn : '1d' })
                    res.send(token)
                } else {
                    res.send("Invalid Username / Password")
                }
            })
        }
    })
})

/* Booking Room */
app.post("/booking", authenticateToken, (req,res) => {
    let user_profile = req.user

    let booker_id = req.query.booker_id
    let room_id = req.query.room_id
    let duration = req.query.duration

    let query = `INSERT INTO Registration
                 (BookerID, Room, Duration, CheckIn)
                 VALUES ('${booker_id}','${room_id}',${duration},NOW())`
    //console.log(query)

    connection.query(query, (err,rows) => {
        if(err) {
            res.json({
                        "status" : "400",
                        "message" : "Error Cannot Booking The Room"
            })
        } else {
            res.json({
                        "status" : "200",
                        "message" : "Booking The Room Success"
            })
        }
    });
})

/* Room For Booking List */
app.get("/list_room", authenticateToken, (req,res) => {
    let user_profile = req.user

    let query = "SELECT * from RoomBook";
    
    connection.query(query, (err,rows) => {
        if(err) {
            res.json({
                        "status" : "400",
                        "message" : "Error Querying From RoomBook"
            })
        } else {
            res.json(rows)
        }
    })
})

/* Adding Room */
app.post("/add_room", (req,res) => {
    let room_name = req.query.room_name

    let query = `INSERT INTO RoomBook
                 (Room)
                 VALUES ('${room_name}')`
    //console.log(query)

    connection.query(query, (err,rows) => {
        if(err) {
            res.json({
                        "status" : "400",
                        "message" : "Error Inserting Data"
            })
        } else {
            res.json({
                        "status" : "200",
                        "message" : "Adding Room Success"
            })
        }
    });
})

/* Update Room */
app.post("/update_room", (req,res) => {
    let room_id = req.query.room_id
    let room_name = req.query.room_name

    let query = `UPDATE RoomBook SET
                 Room='${room_name}'
                 WHERE RoomID=${room_id}`
    //console.log(query)

    connection.query(query, (err,rows) => {
        if(err) {
            res.json({
                        "status" : "400",
                        "message" : "Error Updating Data"
            })
        } else {
            res.json({
                        "status" : "200",
                        "message" : "Updating Room Success"
            })
        }
    });
})

/* Deleting Room */
app.post("/delete_room", (req,res) => {
    let room_id = req.query.room_id

    let query = `DELETE FROM RoomBook WHERE RoomID=${room_id}`
    //console.log(query)

    connection.query(query, (err,rows) => {
        if(err) {
            res.json({
                        "status" : "400",
                        "message" : "Error Deleting Data"
            })
        } else {
            res.json({
                        "status" : "200",
                        "message" : "Deleting Room Success"
            })
        }
    });
})

app.listen(port, () => {
    console.log(`Now Starting Hotel Booking System Backend ${port}`)
})
